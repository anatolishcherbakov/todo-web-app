package by.it_academy.exception;

public class ToDoRepositoryBaseException extends ToDoAppBaseException {

    public ToDoRepositoryBaseException() {
        super();
    }

    public ToDoRepositoryBaseException(String message) {
        super(message);
    }

    public ToDoRepositoryBaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
