package by.it_academy.exception;

public class ToDoAppBaseException extends RuntimeException {

    public ToDoAppBaseException() {
        super();
    }

    public ToDoAppBaseException(String message) {
        super(message);
    }

    public ToDoAppBaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
