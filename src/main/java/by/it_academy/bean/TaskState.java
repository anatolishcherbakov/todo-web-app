package by.it_academy.bean;

public enum TaskState {
    OPEN,
    COMPLETED,
    DELETED
}
