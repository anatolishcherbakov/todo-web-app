package by.it_academy.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@EqualsAndHashCode(exclude = "isProfileEnable")
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table
public class Authenticate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @NotEmpty(message = "{user.login.empty}")
    @Size(min = 3, max = 10, message = "{user.login.invalid}")
    private String login;

    @Column
    @NotEmpty(message = "{user.password.empty}")
    @Size(min = 1, max = 10, message = "{user.password.invalid}")
    private String password;

    @Column(name = "profile_enable")
    private boolean isProfileEnable;
}
