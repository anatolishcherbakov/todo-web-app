package by.it_academy.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@EqualsAndHashCode(exclude = {"fileData", "task"})
@ToString(exclude = {"fileData", "task"})
@NoArgsConstructor

@Entity
@Table(name = "files")
public class TaskFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "file_data")
    private byte[] fileData;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "file")
    private Task task;
}
