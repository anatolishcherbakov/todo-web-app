package by.it_academy.bean;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder
@EqualsAndHashCode(exclude = {"file", "user"})
@ToString(exclude = {"file", "user"})
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column
    private TaskState state;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "file_id")
    private TaskFile file;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "user_id")
    private User user;
}



