package by.it_academy.filter;

import by.it_academy.bean.Role;
import by.it_academy.bean.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.it_academy.util.ApplicationConstant.USER_KEY;

@WebFilter(filterName = "securityFilter", urlPatterns = "/todo/admin/*")
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        HttpSession session = req.getSession(false);
        User user = (User) session.getAttribute(USER_KEY);

        if (user.getRole() == Role.ADMIN) {
            chain.doFilter(request, response);
        } else {
            String URI = req.getContextPath() +"/error";
            resp.sendRedirect(URI);
        }
    }

    @Override
    public void destroy() {

    }
}
