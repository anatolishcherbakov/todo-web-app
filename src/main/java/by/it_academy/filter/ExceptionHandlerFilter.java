package by.it_academy.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "exceptionHandlerFilter", urlPatterns = "/*")
public class ExceptionHandlerFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        try {
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            String URI = req.getContextPath() +"/error";
            resp.sendRedirect(URI);
        }
    }

    @Override
    public void destroy() {

    }
}
