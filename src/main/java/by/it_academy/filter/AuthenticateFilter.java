package by.it_academy.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static by.it_academy.util.ApplicationConstant.USER_KEY;

@WebFilter(filterName = "authenticateFilter", urlPatterns = "/todo/*")
public class AuthenticateFilter implements Filter {

    public void init(FilterConfig config) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);

        if (session != null && session.getAttribute(USER_KEY) != null) {
            chain.doFilter(request, response);
        } else {
            String URI = req.getContextPath() + "/login";
            resp.sendRedirect(URI);
        }
    }

    public void destroy() {
    }
}

