package by.it_academy.util;

public interface ErrorConstant {

    String INVALID_USER_AUTHENTICATION_DATA = "Invalid user authentication data";
    String USER_ALREADY_EXIST = "User with current login already exist";
    String FAILED_START_TCP_SERVER = "Failed start tcp H2 server";
    String ATTACH_FILE_TO_TASK_ERROR = "*attach file to task error..";
    String DOWNLOAD_FILE_ERROR = "*download file process error..";

    String TASK_NOT_FOUND = "Task with current id not found. Id = ";
    String FILE_NOT_FOUND = "File with current id not found. Id = ";
    String USER_NOT_FOUND = "User with current id not found. Id = ";
}
