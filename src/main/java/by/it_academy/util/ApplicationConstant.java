package by.it_academy.util;

import java.time.LocalDate;

public interface ApplicationConstant {

    String TITLE_KEY = "title";
    String ERROR_KEY = "error";
    String AUTHENTICATE_KEY = "authenticate";
    String USER_KEY = "user";
    String USERS_KEY = "users";
    String MESSAGES_KEY = "messages";
    String ROLES_KEY = "roles";
    String TASK_KEY = "task";
    String DATE_KEY = "date";
    String TASKS_KEY = "tasks";
    String MARK_TASK_LIST_KEY = "mark";
    String TODAY_KEY = "today";
    String TOMORROW_KEY = "tomorrow";
    String SOMEDAY_KEY = "some";
    String COMPLETED_KEY = "completed";
    String DELETED_KEY = "deleted";

    String ADD_USER_PAGE_TITLE = "Add user";
    String ADD_NEW_TASK_TITLE = "Add new task";
    String EDIT_TASK_TITLE = "Edit task";
    String VIEW_MESSAGES_TITLE = "View messages";
    String USER_LIST_TITLE = "User list";

    String MAIN_PAGE = "main";
    String LOGIN_PAGE = "login";
    String REGISTRATION_PAGE = "registration";
    String ERROR_PAGE = "error";
    String ADD_NEW_USER_PAGE = "add_user";
    String EDIT_PROFILE_PAGE = "edit_profile";
    String VIEW_MESSAGES_PAGE = "view_messages";
    String CREATE_TASKS_PAGE = "create_task";
    String SEND_MESSAGE_PAGE = "send_message";
    String PROFILE_PAGE = "profile";

    String REDIRECT_TO_USER_MAIN_PAGE = "redirect:/todo/tasks/today";
    String REDIRECT_TO_ADMIN_MAIN_PAGE = "redirect:/todo/admin/users";
    String REDIRECT_TO_TASKS_LIST_PAGE = "redirect:/todo/tasks/";
    String REDIRECT_TO_PROFILE_PAGE = "redirect:/todo/profile";
    String REDIRECT_TO_LOGIN_PAGE = "redirect:/login";

    LocalDate FAKE_DATE = LocalDate.parse("2222-11-11");
    LocalDate TODAY_DATE = LocalDate.now();
    LocalDate TOMORROW_DATE = LocalDate.now().plusDays(1);
    LocalDate AFTER_TOMORROW_DATE = LocalDate.now().plusDays(2);
}
