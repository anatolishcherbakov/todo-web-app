package by.it_academy.controller;

import by.it_academy.bean.Message;
import by.it_academy.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

import static by.it_academy.util.ApplicationConstant.*;

@Controller
@RequestMapping("/todo")
public class MessageController {

    private MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/admin/messages")
    public String showMessages(Model model) {
        List<Message> messages =  messageService.findAll();
        model.addAttribute(MESSAGES_KEY, messages);
        return VIEW_MESSAGES_PAGE;
    }

    @PostMapping("/admin/messages/delete/{id}")
    public String deleteMessage(@PathVariable Long id, Model model) {
        messageService.deleteById(id);
        List<Message> messages = messageService.findAll();
        model.addAttribute(MESSAGES_KEY, messages);
        return VIEW_MESSAGES_PAGE;
    }

    @PostMapping("/message/send")
    public String sendMessage(@RequestParam("message") String stringMessage,
                              @RequestParam String email, HttpSession session) {
        Message message = new Message();
        message.setMessage(stringMessage);
        message.setUserEmail(email);
        messageService.save(message);
        session.invalidate();
        return REDIRECT_TO_LOGIN_PAGE;
    }
}
