package by.it_academy.controller;

import by.it_academy.bean.Task;
import by.it_academy.bean.TaskFile;
import by.it_academy.exception.ToDoAppBaseException;
import by.it_academy.service.FileService;
import by.it_academy.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static by.it_academy.util.ApplicationConstant.REDIRECT_TO_TASKS_LIST_PAGE;
import static by.it_academy.util.ErrorConstant.DOWNLOAD_FILE_ERROR;

@Controller
@RequestMapping("/todo/tasks")
public class FileController {

    private TaskService taskService;
    private FileService fileService;

    @Autowired
    public FileController(TaskService taskService, FileService fileService) {
        this.taskService = taskService;
        this.fileService = fileService;
    }

    @PostMapping("/{taskId}/{mark}/attach/file")
    public String addFileToTask(@PathVariable Long taskId, @PathVariable String mark,
                                @RequestParam MultipartFile file) {
        Task task = taskService.findById(taskId);
        taskService.attachFileToTaskFromMultipart(file, task);
        taskService.save(task);
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }

    @ResponseBody
    @PostMapping("/{mark}/download/file/{fileId}")
    public String downloadFile(@PathVariable String mark, @PathVariable Long fileId,
                               HttpServletRequest request, HttpServletResponse response) {
        TaskFile taskFile = fileService.findById(fileId);
        String fileName = taskFile.getName();
        String contentType = request.getServletContext().getMimeType(fileName);

        response.setHeader("Content-Type", contentType);
        response.setHeader("Content-Length", String.valueOf(taskFile.getFileData().length));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + taskFile.getName() + "\"");

        byte[] fileData = taskFile.getFileData();
        InputStream is = new ByteArrayInputStream(fileData);
        byte[] bytes = new byte[fileData.length];
        try {
            int bytesRead;
            while ((bytesRead = is.read(bytes)) != -1) {
                response.getOutputStream().write(bytes, 0, bytesRead);
            }
        } catch (IOException e) {
            throw new ToDoAppBaseException(DOWNLOAD_FILE_ERROR);
        }
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }

    @PostMapping("/{taskId}/{mark}/delete/file")
    public String deleteFileFromTask(@PathVariable Long taskId, @PathVariable String mark) {
        Task task = taskService.findById(taskId);
        task.setFile(null);
        taskService.save(task);
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }
}
