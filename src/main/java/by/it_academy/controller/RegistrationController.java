package by.it_academy.controller;

import by.it_academy.bean.Authenticate;
import by.it_academy.bean.Role;
import by.it_academy.bean.User;
import by.it_academy.exception.ToDoAppBaseException;
import by.it_academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static by.it_academy.util.ApplicationConstant.*;
import static by.it_academy.util.ErrorConstant.USER_ALREADY_EXIST;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String showRegistration(Model model) {
        User user = new User();
        user.setRole(Role.USER);
        Authenticate authenticate = new Authenticate();
        authenticate.setProfileEnable(true);
        user.setAuthenticate(authenticate);
        model.addAttribute(USER_KEY, user);

        model.addAttribute(ROLES_KEY, Role.values());

        return REGISTRATION_PAGE;
    }

    @PostMapping
    public String registrationProcess(@Valid @ModelAttribute User user,
                                      BindingResult result, HttpSession session) {
        if (result.hasErrors()) {
            return REGISTRATION_PAGE;
        }
        try {
            String login = user.getAuthenticate().getLogin().trim();
            if (userService.existsByLogin(login)) {
                throw new ToDoAppBaseException(USER_ALREADY_EXIST);
            }
            user.getAuthenticate().setLogin(login);
            user = userService.save(user);
            session.setAttribute(USER_KEY, user);

        } catch (RuntimeException e) {
            session.setAttribute(ERROR_KEY, e.getMessage());
            return REGISTRATION_PAGE;
        }
        return REDIRECT_TO_USER_MAIN_PAGE;
    }
}
