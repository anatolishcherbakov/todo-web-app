package by.it_academy.controller;

import by.it_academy.bean.Authenticate;
import by.it_academy.bean.Role;
import by.it_academy.bean.User;
import by.it_academy.exception.ToDoAppBaseException;
import by.it_academy.service.AuthenticateService;
import by.it_academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static by.it_academy.util.ApplicationConstant.*;
import static by.it_academy.util.ErrorConstant.INVALID_USER_AUTHENTICATION_DATA;

@Controller
@RequestMapping({"/", "/login"})
public class LoginController {

    private UserService userService;
    private AuthenticateService authenticateService;

    @Autowired
    public LoginController(UserService userService, AuthenticateService authenticateService) {
        this.userService = userService;
        this.authenticateService = authenticateService;
    }

    @GetMapping
    public String showLogin(Model model) {
        Authenticate authenticate = new Authenticate();
        model.addAttribute(AUTHENTICATE_KEY, authenticate);
        return LOGIN_PAGE;
    }

    @PostMapping
    public String loginProcess(@Valid @ModelAttribute Authenticate authenticate,
                               BindingResult result, HttpSession session) {
        if (result.hasErrors()) {
            return LOGIN_PAGE;
        }
        String login = authenticate.getLogin();
        String password = authenticate.getPassword();
        try {
            if (!userService.existsByLogin(login)) {
                throw new ToDoAppBaseException(INVALID_USER_AUTHENTICATION_DATA);
            }
            User user = userService.getByLoginAndPassword(login, password);
            if (user == null) {
                throw new ToDoAppBaseException(INVALID_USER_AUTHENTICATION_DATA);
            }
            session.setAttribute(USER_KEY, user);

            if (!authenticateService.isUserProfileEnable(login)) {
                return SEND_MESSAGE_PAGE;
            }
            if (user.getRole() == Role.USER) {
                return REDIRECT_TO_USER_MAIN_PAGE;
            } else {
                return REDIRECT_TO_ADMIN_MAIN_PAGE;
            }
        } catch (RuntimeException e) {
            session.setAttribute(ERROR_KEY, e.getMessage());
            return LOGIN_PAGE;
        }
    }
}
