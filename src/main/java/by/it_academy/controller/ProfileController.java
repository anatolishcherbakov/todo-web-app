package by.it_academy.controller;

import by.it_academy.bean.User;
import by.it_academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static by.it_academy.util.ApplicationConstant.*;

@Controller
@RequestMapping("/todo/profile")
public class ProfileController {

    private UserService userService;

    @Autowired
    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String showProfileInfo() {
        return PROFILE_PAGE;
    }

    @PostMapping
    public String editProfilePage(@SessionAttribute User user, Model model) {
        model.addAttribute(USER_KEY, user);
        return EDIT_PROFILE_PAGE;
    }

    @PostMapping("/edit")
    public String editProfileProcess(@Valid @ModelAttribute User user, BindingResult result, HttpSession session) {
        if (result.hasErrors()) {
            return EDIT_PROFILE_PAGE;
        }
        userService.save(user);
        session.setAttribute(USER_KEY, user);
        return REDIRECT_TO_PROFILE_PAGE;
    }
}
