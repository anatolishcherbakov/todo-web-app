package by.it_academy.controller;

import by.it_academy.bean.Authenticate;
import by.it_academy.bean.Role;
import by.it_academy.bean.User;
import by.it_academy.exception.ToDoAppBaseException;
import by.it_academy.service.AuthenticateService;
import by.it_academy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

import static by.it_academy.util.ApplicationConstant.*;
import static by.it_academy.util.ErrorConstant.USER_ALREADY_EXIST;

@Controller
@RequestMapping("/todo/admin/users")
public class UserController {

    private UserService userService;
    private AuthenticateService authenticateService;

    @Autowired
    public UserController(UserService userService, AuthenticateService authenticateService) {
        this.userService = userService;
        this.authenticateService = authenticateService;
    }

    @GetMapping
    public String showUsersList(Model model) {
        List<User> users = userService.findAll();
        model.addAttribute(USERS_KEY, users);
        return MAIN_PAGE;
    }

    @PostMapping("/{userId}/block")
    public String blockUserAccount(@PathVariable Long userId) {
        authenticateService.blockUnblockUser(false, userId);
        return REDIRECT_TO_ADMIN_MAIN_PAGE;
    }

    @PostMapping("/{userId}/unblock")
    public String unblockUserAccount(@PathVariable Long userId) {
        authenticateService.blockUnblockUser(true, userId);
        return REDIRECT_TO_ADMIN_MAIN_PAGE;
    }

    @PostMapping("/{userId}/delete")
    public String deleteUser(@PathVariable Long userId) {
        userService.deleteById(userId);
        return REDIRECT_TO_ADMIN_MAIN_PAGE;
    }

    @GetMapping("/add")
    public String addNewUserPage(Model model) {
        User user = new User();
        Authenticate authenticate = new Authenticate();
        authenticate.setProfileEnable(true);
        user.setAuthenticate(authenticate);
        model.addAttribute(USER_KEY, user);
        model.addAttribute(ROLES_KEY, Role.values());
        return ADD_NEW_USER_PAGE;
    }

    @PostMapping("/add")
    public String addNewUserProcess(@Valid @ModelAttribute User user, BindingResult result,
                                    HttpSession session, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(ROLES_KEY, Role.values());
            return ADD_NEW_USER_PAGE;
        }
        try {
            String login = user.getAuthenticate().getLogin().trim();
            if (userService.existsByLogin(login)) {
                throw new ToDoAppBaseException(USER_ALREADY_EXIST);
            }
            user.getAuthenticate().setLogin(login);
            userService.save(user);
            
        } catch (RuntimeException e) {
            session.setAttribute(ERROR_KEY, e.getMessage());
            model.addAttribute(ROLES_KEY, Role.values());
            return ADD_NEW_USER_PAGE;
        }
        return REDIRECT_TO_ADMIN_MAIN_PAGE;
    }
}
