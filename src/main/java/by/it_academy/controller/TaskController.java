package by.it_academy.controller;

import by.it_academy.bean.Task;
import by.it_academy.bean.TaskState;
import by.it_academy.bean.User;
import by.it_academy.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

import static by.it_academy.util.ApplicationConstant.*;

@Controller
@RequestMapping("/todo/tasks")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/{mark}")
    public String showTasks(@PathVariable String mark, @SessionAttribute User user,
                            Model model) {
        List<Task> tasks = taskService.getAllByTaskListMarkAndUser(mark, user);
        model.addAttribute(MARK_TASK_LIST_KEY, mark);
        model.addAttribute(TASKS_KEY, tasks);
        return MAIN_PAGE;
    }

    @PostMapping("/{mark}")
    public String createTaskPage(@PathVariable String mark, Model model) {
        model.addAttribute(TITLE_KEY, ADD_NEW_TASK_TITLE);
        if (SOMEDAY_KEY.equals(mark)) {
            model.addAttribute(DATE_KEY, AFTER_TOMORROW_DATE);
        }
        return CREATE_TASKS_PAGE;
    }

    @PostMapping("/{mark}/create")
    public String createTaskProcess(@PathVariable String mark, @SessionAttribute User user,
                                    @RequestParam MultipartFile file, @RequestParam String name,
                                    @RequestParam String description) {
        LocalDate date;
        if (TODAY_KEY.equals(mark)) {
            date = TODAY_DATE;
        } else if (TOMORROW_KEY.equals(mark)) {
            date = TOMORROW_DATE;
        } else {
            date = AFTER_TOMORROW_DATE;
        }
        Task task = new Task();
        taskService.attachFileToTaskFromMultipart(file, task);
        task.setName(name);
        task.setDescription(description);
        task.setState(TaskState.OPEN);
        task.setDate(date);
        task = taskService.save(task);
        task.setUser(user);
        taskService.save(task);
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }

    @PostMapping("/{taskId}/{mark}")
    public String editTaskPage(@PathVariable Long taskId, @PathVariable String mark,
                               Model model) {
        Task task = taskService.findById(taskId);
        model.addAttribute(TITLE_KEY, EDIT_TASK_TITLE);
        model.addAttribute(MARK_TASK_LIST_KEY, mark);
        model.addAttribute(TASK_KEY, task);
        return CREATE_TASKS_PAGE;
    }

    @PostMapping("/{taskId}/{mark}/edit")
    public String editTaskProcess(@PathVariable Long taskId, @PathVariable String mark,
                                  @RequestParam String name, @RequestParam String description,
                                  @RequestParam String date) {
        LocalDate taskDate = date.isEmpty() ? FAKE_DATE : LocalDate.parse(date);
        Task task = taskService.findById(taskId);
        task.setName(name);
        task.setDescription(description);
        task.setDate(taskDate);
        taskService.save(task);
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }

    @PostMapping("/{taskId}/{mark}/change_state/{state}")
    public String changeTaskState(@PathVariable Long taskId, @PathVariable String mark,
                                  @PathVariable String state) {
        TaskState taskState = TaskState.valueOf(state.toUpperCase());
        taskService.changeTaskState(taskState, taskId);
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }

    @PostMapping("/{mark}/delete/{taskId}")
    public String deleteTask(@PathVariable String mark, @PathVariable Long taskId) {
        taskService.deleteById(taskId);
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }

    @PostMapping("/{mark}/clean")
    public String cleanDeletedTasks(@PathVariable String mark, @SessionAttribute User user) {
        List<Task> tasks = taskService.getAllByTaskListMarkAndUser(mark, user);
        for (Task task : tasks) {
            taskService.deleteById(task.getId());
        }
        return REDIRECT_TO_TASKS_LIST_PAGE + mark;
    }
}
