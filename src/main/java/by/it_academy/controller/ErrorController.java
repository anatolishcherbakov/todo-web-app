package by.it_academy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static by.it_academy.util.ApplicationConstant.ERROR_PAGE;

@Controller
@RequestMapping("/error")
public class ErrorController {

    @GetMapping
    public String showErrorPage() {
        return ERROR_PAGE;
    }
}
