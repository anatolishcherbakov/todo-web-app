package by.it_academy.repository;

import by.it_academy.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User getByAuthenticate_LoginAndAuthenticate_Password(String login, String password);

    boolean existsByAuthenticate_Login(String login);
}
