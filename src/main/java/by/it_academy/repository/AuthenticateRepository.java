package by.it_academy.repository;

import by.it_academy.bean.Authenticate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AuthenticateRepository extends JpaRepository<Authenticate, Long> {

    @Transactional
    @Query(nativeQuery = true, value = "select PROFILE_ENABLE from AUTHENTICATE where LOGIN = ?")
    boolean isUserProfileEnable(String login);

    @Transactional
    @Modifying
    @Query(nativeQuery = true, value = "update AUTHENTICATE set PROFILE_ENABLE = ? where ID = ?")
    void blockUnblockUser(boolean isProfileEnable, long id);
}
