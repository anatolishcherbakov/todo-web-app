package by.it_academy.repository;

import by.it_academy.bean.TaskFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends JpaRepository<TaskFile, Long> {
}
