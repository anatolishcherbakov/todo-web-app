package by.it_academy.repository;

import by.it_academy.bean.Task;
import by.it_academy.bean.TaskState;
import by.it_academy.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Transactional
    @Modifying
    @Query("update Task set state = :state where id = :id")
    void changeTaskState(@Param("state") TaskState state, @Param("id") long id);

    List<Task> getAllByDateAndStateAndUser(LocalDate date, TaskState state, User user);

    List<Task> getAllByDateLessThanEqualAndStateAndUser(LocalDate date, TaskState state, User user);

    List<Task> getAllByDateGreaterThanEqualAndStateAndUser(LocalDate date, TaskState state, User user);

    List<Task> getAllByStateAndUser(TaskState state, User user);
}
