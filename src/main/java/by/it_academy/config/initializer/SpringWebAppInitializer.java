package by.it_academy.config.initializer;

import by.it_academy.config.AppConfig;
import by.it_academy.config.WebConfig;
import by.it_academy.filter.AuthenticateFilter;
import by.it_academy.filter.CharsetFilter;
import by.it_academy.filter.ExceptionHandlerFilter;
import by.it_academy.filter.SecurityFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class SpringWebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
       servletContext.addFilter("authenticateFilter", AuthenticateFilter.class);
       servletContext.addFilter("charsetFilter", CharsetFilter.class);
       servletContext.addFilter("exceptionHandlerFilter", ExceptionHandlerFilter.class);
       servletContext.addFilter("securityFilter", SecurityFilter.class);
        super.onStartup(servletContext);
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{AppConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
