package by.it_academy.config;

import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.SQLException;

import static by.it_academy.util.ErrorConstant.FAILED_START_TCP_SERVER;

@Configuration
public class AppConfig {

    @Bean
    public Server tcpServer() {
        try {
           return Server.createTcpServer().start();
        } catch (SQLException e) {
            throw new RuntimeException(FAILED_START_TCP_SERVER);
        }
    }
}
