package by.it_academy.service;

import by.it_academy.repository.AuthenticateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticateService {

    private AuthenticateRepository authenticateRepository;

    @Autowired
    public AuthenticateService(AuthenticateRepository authenticateRepository) {
        this.authenticateRepository = authenticateRepository;
    }

    public boolean isUserProfileEnable(String login) {
        return authenticateRepository.isUserProfileEnable(login);
    }

    public void blockUnblockUser(boolean isProfileEnable, long id) {
        authenticateRepository.blockUnblockUser(isProfileEnable, id);
    }
}
