package by.it_academy.service;

import by.it_academy.bean.Task;
import by.it_academy.bean.TaskFile;
import by.it_academy.bean.TaskState;
import by.it_academy.bean.User;
import by.it_academy.exception.ToDoAppBaseException;
import by.it_academy.exception.ToDoRepositoryBaseException;
import by.it_academy.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static by.it_academy.util.ApplicationConstant.*;
import static by.it_academy.util.ErrorConstant.ATTACH_FILE_TO_TASK_ERROR;
import static by.it_academy.util.ErrorConstant.TASK_NOT_FOUND;

@Service
public class TaskService {

    private TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task save(Task task) {
        return taskRepository.save(task);
    }

    public Task findById(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new ToDoRepositoryBaseException(TASK_NOT_FOUND));
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public void deleteById(Long id) {
        taskRepository.deleteById(id);
    }

    public void changeTaskState(TaskState state, long id) {
        taskRepository.changeTaskState(state, id);
    }

    public List<Task> getAllByTaskListMarkAndUser(String mark, User user) {
        if (mark.equals(TODAY_KEY) || mark.equals(TOMORROW_KEY) || mark.equals(SOMEDAY_KEY)) {
            if (mark.equals(TODAY_KEY)) {
                return taskRepository.getAllByDateLessThanEqualAndStateAndUser(TODAY_DATE, TaskState.OPEN, user);
            } else if (mark.equals(TOMORROW_KEY)) {
                return taskRepository.getAllByDateAndStateAndUser(TOMORROW_DATE, TaskState.OPEN, user);
            } else {
                return taskRepository.getAllByDateGreaterThanEqualAndStateAndUser(AFTER_TOMORROW_DATE, TaskState.OPEN, user);
            }

        } else {
            TaskState state = TaskState.valueOf(mark.toUpperCase());
            return taskRepository.getAllByStateAndUser(state, user);
        }
    }

    public void attachFileToTaskFromMultipart(MultipartFile file, Task task) {
        try {
            String fileName = file.getOriginalFilename();
            if (!fileName.isEmpty()) {
                byte[] fileData = file.getBytes();
                TaskFile taskFile = new TaskFile();
                taskFile.setName(fileName);
                taskFile.setFileData(fileData);
                task.setFile(taskFile);
            }
        } catch (IOException e) {
            throw new ToDoAppBaseException(ATTACH_FILE_TO_TASK_ERROR);
        }
    }
}
