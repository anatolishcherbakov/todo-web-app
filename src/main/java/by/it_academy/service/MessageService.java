package by.it_academy.service;

import by.it_academy.bean.Message;
import by.it_academy.exception.ToDoRepositoryBaseException;
import by.it_academy.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static by.it_academy.util.ErrorConstant.USER_NOT_FOUND;

@Service
public class MessageService {

    private MessageRepository messageRepository;

    @Autowired
    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }

    public Message findById(Long id) {
        return messageRepository.findById(id)
                .orElseThrow(() -> new ToDoRepositoryBaseException(USER_NOT_FOUND));
    }

    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public void deleteById(Long id) {
        messageRepository.deleteById(id);
    }
}
