package by.it_academy.service;

import by.it_academy.bean.TaskFile;
import by.it_academy.exception.ToDoRepositoryBaseException;
import by.it_academy.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static by.it_academy.util.ErrorConstant.FILE_NOT_FOUND;

@Service
public class FileService {

    private FileRepository fileRepository;

    @Autowired
    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public TaskFile save(TaskFile file) {
        return fileRepository.save(file);
    }

    public TaskFile findById(Long id) {
        return fileRepository.findById(id)
                .orElseThrow(() -> new ToDoRepositoryBaseException(FILE_NOT_FOUND));
    }

    public List<TaskFile> findAll() {
        return fileRepository.findAll();
    }

    public void deleteById(Long id) {
        fileRepository.deleteById(id);
    }

}
