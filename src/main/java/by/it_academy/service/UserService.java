package by.it_academy.service;

import by.it_academy.bean.User;
import by.it_academy.exception.ToDoRepositoryBaseException;
import by.it_academy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static by.it_academy.util.ErrorConstant.USER_NOT_FOUND;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    public User findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new ToDoRepositoryBaseException(USER_NOT_FOUND));
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    public User getByLoginAndPassword(String login, String password) {
        return userRepository.getByAuthenticate_LoginAndAuthenticate_Password(login, password);
    }

    public boolean existsByLogin(String login) {
        return userRepository.existsByAuthenticate_Login(login);
    }
}
