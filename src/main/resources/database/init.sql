insert into authenticate (login, password, profile_enable) values ('admin1', '1', true);
insert into authenticate (login, password, profile_enable) values ('admin2', '2', true);

insert into users (name, surname, email, role) values ('admin1', 'admin1', 'admin1@admin.com', 'ADMIN');
insert into users (name, surname, email, role) values ('admin2', 'admin2', 'admin2@admin.com', 'ADMIN');