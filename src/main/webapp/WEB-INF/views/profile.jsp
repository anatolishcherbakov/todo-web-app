<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.it_academy.util.ApplicationConstant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="by.it_academy.bean.Role" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>

    <style>
        <%@ include file="/css/style.css" %>
        <%@ include file="/css/main_page_style.css" %>
    </style>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous"/>

</head>

<body>
<div class="colorlib-loader"></div>
<div id="page">
    <nav class="colorlib-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="colorlib-logo">
                            <c:choose>
                                <c:when test="${sessionScope.user.role == Role.USER}">
                                    <form action="<c:url value="/todo/tasks/today"/>" method="get">
                                        <button class="logo">to_do</button>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form action="<c:url value="/todo/admin/users"/>" method="get">
                                        <button class="logo">admin_panel</button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li class="has-dropdown active">
                                <a href="<c:url value="/todo/profile"/>">profile</a>
                            </li>
                            <c:if test="${sessionScope.user.role == Role.USER}">
                                <li>
                                    <a href="<c:url value="/todo/tasks/today"/>"><%= ApplicationConstant.TODAY_KEY%></a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/tomorrow"/>"><%= ApplicationConstant.TOMORROW_KEY%></a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/some"/>"><%= ApplicationConstant.SOMEDAY_KEY%></a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/completed"/>"><%= ApplicationConstant.COMPLETED_KEY%></a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/deleted"/>"><%= ApplicationConstant.DELETED_KEY%></a>
                                </li>
                            </c:if>
                            <c:if test="${sessionScope.user.role == Role.ADMIN}">
                                <li>
                                    <a href="<c:url value="/todo/admin/users"/>"><%= ApplicationConstant.USER_LIST_TITLE %></a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/admin/messages"/>"><%= ApplicationConstant.VIEW_MESSAGES_TITLE %></a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/admin/users/add"/>"><%= ApplicationConstant.ADD_USER_PAGE_TITLE %></a>
                                </li>
                            </c:if>

                            <li class="active">
                                <a href="<c:url value="/logout"/>">logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="colorlib-container">
        <div class="container">
            <p class="title">profile</p>
            <table>
                <tr>
                    <td>_login</td>
                    <td>${sessionScope.user.authenticate.login}</td>
                </tr>
                <tr>
                    <td>_name</td>
                    <td>
                        ${sessionScope.user.name}
                        <c:if test="${empty sessionScope.user.name}">-</c:if>
                    </td>
                </tr>
                <tr>
                    <td>_surname</td>
                    <td>
                        ${sessionScope.user.surname}
                        <c:if test="${empty sessionScope.user.surname}">-</c:if>
                    </td>
                </tr>
                <tr>
                    <td>_email</td>
                    <td>
                        ${sessionScope.user.email}
                        <c:if test="${empty sessionScope.user.email}">-</c:if>
                    </td>
                </tr>
                <tr>
                    <td>_role</td>
                    <td>${sessionScope.user.role}</td>
                </tr>
            </table>

            <div class="add-task">
                <form action="<c:url value="/todo/profile"/>" method="post">
                    <button class="button add"><i class="fas fa-user-edit"></i> Edit profile</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>