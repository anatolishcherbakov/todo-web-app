<%@ page import="by.it_academy.util.ApplicationConstant" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Edit profile</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        <%@ include file="/css/form_style.css" %>
    </style>
</head>
<body>


<div class="login-page">
    <div class="form">
        <form:form class="register-form" action="${pageContext.request.contextPath}/todo/profile/edit"
                   modelAttribute="user" method="post">

            <p class="title">Edit profile</p>

            <jsp:include page="../../template/error_tmpl.jsp"/>

            <p class="alignleft">login:</p>
            <form:errors path="authenticate.login" cssStyle="color: red"/>
            <form:input path="authenticate.login" value="${sessionScope.user.authenticate.login}" type="text"
                        placeholder="_login"/>

            <p class="alignleft">password:</p>
            <form:errors path="authenticate.password" cssStyle="color: red"/>
            <form:input path="authenticate.password" value="${sessionScope.user.authenticate.password}" type="text"
                        placeholder="_password"/>

            <p class="alignleft">name:</p>
            <form:errors path="name" cssStyle="color: red"/>
            <form:input path="name" value="${sessionScope.user.name}" type="text" placeholder="_name"/>

            <p class="alignleft">surname:</p>
            <form:errors path="surname" cssStyle="color: red"/>
            <form:input path="surname" value="${sessionScope.user.surname}" type="text" placeholder="_surname"/>

            <p class="alignleft">email:</p>
            <form:errors path="email" cssStyle="color: red"/>
            <form:input path="email" value="${sessionScope.user.email}" type="email" placeholder="_email"/>

            <form:hidden path="id" value="${user.id}"/>
            <form:hidden path="role" value="${user.role}"/>
            <form:hidden path="authenticate.id" value="${user.authenticate.id}"/>
            <form:hidden path="authenticate.profileEnable" value="${user.authenticate.profileEnable}"/>

            <button>Edit</button>
            <p class="message"><a href="<c:url value="/todo/profile"/>"><-- main page</a></p>
        </form:form>
    </div>
</div>

</body>
</html>
