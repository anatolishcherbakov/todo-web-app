<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="by.it_academy.util.ApplicationConstant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="by.it_academy.bean.Role" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To-Do List</title>

    <style>
        <%@ include file="/css/style.css" %>
        <%@ include file="/css/main_page_style.css" %>
    </style>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous"/>

</head>
<body>

<div class="colorlib-loader"></div>

<div id="page">
    <nav class="colorlib-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="colorlib-logo">
                            <c:choose>
                                <c:when test="${sessionScope.user.role == Role.USER}">
                                    <form action="<c:url value="/todo/tasks/today"/>" method="get">
                                        <button class="logo">to_do</button>
                                    </form>
                                </c:when>
                                <c:otherwise>
                                    <form action="<c:url value="/todo/admin/users"/>" method="get">
                                        <button class="logo">admin_panel</button>
                                    </form>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li class="has-dropdown active">
                                <a href="<c:url value="/todo/profile"/>">profile</a>
                            </li>
                            <c:if test="${sessionScope.user.role == Role.USER}">
                                <li>
                                    <a href="<c:url value="/todo/tasks/today"/>"><%= ApplicationConstant.TODAY_KEY%>
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/tomorrow"/>"><%= ApplicationConstant.TOMORROW_KEY%>
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/some"/>"><%= ApplicationConstant.SOMEDAY_KEY%>
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/completed"/>"><%= ApplicationConstant.COMPLETED_KEY%>
                                    </a>
                                </li>
                                <li>
                                    <a href="<c:url value="/todo/tasks/deleted"/>"><%= ApplicationConstant.DELETED_KEY%>
                                    </a>
                                </li>
                            </c:if>
                            <c:if test="${sessionScope.user.role == Role.ADMIN}">
                                <li>
                                    <a href="<c:url value="/todo/admin/users"/>"><%= ApplicationConstant.USER_LIST_TITLE %></a>
                                </li>

                                <li>
                                    <a href="<c:url value="/todo/admin/messages"/>"><%= ApplicationConstant.VIEW_MESSAGES_TITLE %></a>
                                </li>

                                <li>
                                    <a href="<c:url value="/todo/admin/users/add"/>"><%= ApplicationConstant.ADD_USER_PAGE_TITLE %></a>
                                </li>
                            </c:if>

                            <li class="active">
                                <a href="<c:url value="/logout"/>">logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <div id="colorlib-container">
        <div class="container">
            <c:if test="${sessionScope.user.role == Role.USER}">
                <p class="title">${mark}</p>
                <c:if test="${tasks.size() > 0}">
                    <table>
                        <tr>
                            <th class="coll"></th>
                            <th>Name</th>
                            <th>Definition</th>
                            <th>Date</th>
                            <c:if test="${!(fn:contains(mark, 'deleted') || fn:contains(mark, 'completed'))}">
                                <th>File</th>
                                <th></th>
                            </c:if>
                            <th class="coll"></th>
                        </tr>

                        <c:forEach items="${tasks}" var="task" varStatus="status">
                            <tr>
                                <c:if test="${mark == 'today' || mark == 'tomorrow' || mark == 'some'}">
                                    <td>
                                        <form class="checkform"
                                              action="<c:url value="/todo/tasks/${task.id}/${mark}/change_state/completed"/>"
                                              method="post">
                                            <button class="button check"><i class="far fa-check-circle"></i></button>
                                        </form>
                                    </td>
                                </c:if>

                                <c:if test="${mark == 'completed' || mark == 'deleted'}">
                                    <td>
                                        <form class="rollback"
                                              action="<c:url value="/todo/tasks/${task.id}/${mark}/change_state/open"/>"
                                              method="post">
                                            <button class="button undo"><i class="fas fa-undo-alt"></i></button>
                                        </form>
                                    </td>
                                </c:if>

                                <td>${task.getName()}
                                    <c:if test="${empty task.getName()}">no_name</c:if></td>
                                <td>${task.getDescription()}
                                    <c:if test="${empty task.getDescription()}">no_description</c:if></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${task.getDate() == '2222-11-11'}">no_date</c:when>
                                        <c:otherwise>${task.getDate()}</c:otherwise>
                                    </c:choose>
                                </td>

                                <c:if test="${mark == 'today' || mark == 'tomorrow' || mark == 'some'}">
                                    <td>
                                        <c:if test="${task.file == null}">
                                            <form method="post" enctype="multipart/form-data"
                                                  action="<c:url value="/todo/tasks/${task.id}/${mark}/attach/file"/>">
                                                <input type="file" name="file" id="file-${status.count}"
                                                       class="inputfile"
                                                       onchange="this.form.submit()">
                                                <label for="file-${status.count}"><i class="fas fa-upload"></i></label>
                                            </form>
                                        </c:if>
                                        <c:if test="${task.file != null}">
                                            <form class="formfile" method="post"
                                                  action="<c:url value="/todo/tasks/${mark}/download/file/${task.file.id}"/>">
                                                <button class="download"><i class="fas fa-file-download"></i></button>
                                            </form>

                                            <form class="formfile" method="post"
                                                  action="<c:url value="/todo/tasks/${task.id}/${mark}/delete/file"/>">
                                                <button class="download"><i class="fas fa-times"></i></button>
                                            </form>
                                        </c:if>
                                    </td>

                                    <td>
                                        <form method="post" action="<c:url value="/todo/tasks/${task.id}/${mark}"/>">
                                            <button class="button pencil"><i class="fas fa-pencil-alt"></i></button>
                                        </form>
                                    </td>
                                </c:if>

                                <c:if test="${mark == 'today' || mark == 'tomorrow' || mark == 'some' || mark == 'completed'}">
                                    <td>
                                        <form method="post"
                                              action="<c:url value="/todo/tasks/${task.id}/${mark}/change_state/deleted"/>">
                                            <button class="button trash"><i class="far fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </c:if>

                                <c:if test="${mark == 'deleted'}">
                                    <td>
                                        <form method="post" action="<c:url value="/todo/tasks/${mark}/delete/${task.id}"/>">
                                            <button class="button trash"><i class="far fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>

                <c:if test="${mark == 'today' || mark == 'tomorrow' || mark == 'some'}">
                    <div class="add-task">
                        <form action="<c:url value="/todo/tasks/${mark}"/>" method="post">
                            <button class="button add"><i class="fas fa-plus"></i> Add task</button>
                        </form>
                    </div>
                </c:if>

                <c:if test="${mark == 'deleted'}">
                    <div class="delete-all">
                        <form action="<c:url value="/todo/tasks/${mark}/clean"/>" method="post">
                            <button class="button clean-all"><i class="fas fa-broom"></i> Clean all</button>
                        </form>
                    </div>
                </c:if>
            </c:if>

            <c:if test="${sessionScope.user.role == Role.ADMIN}">
                <p class="title">user list</p>
                <c:if test="${users.size() > 0}">
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Login</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th class="coll"></th>
                            <th class="coll"></th>
                        </tr>

                        <c:forEach items="${users}" var="user">
                            <tr>
                                <td>${user.getId()}</td>
                                <td>${user.getAuthenticate().getLogin()}</td>
                                <td>${user.getName()}</td>
                                <td>${user.getSurname()}</td>
                                <td>${user.getEmail()}</td>
                                <td>${user.getRole()}</td>

                                <td>
                                    <c:if test="${sessionScope.user.id != user.id && user.authenticate.isProfileEnable() == true}">
                                        <form action="<c:url value="/todo/admin/users/${user.id}/block"/>" method="post">
                                            <button class="button unblock"><i class="fa fa-unlock-alt"></i></button>
                                        </form>
                                    </c:if>

                                    <c:if test="${sessionScope.user.id != user.id && user.authenticate.isProfileEnable() == false}">
                                        <form action="<c:url value="/todo/admin/users/${user.id}/unblock"/>" method="post">
                                            <button class="button block"><i class="fa fa-lock"></i>
                                            </button>
                                        </form>
                                    </c:if>
                                </td>

                                <td>
                                    <c:if test="${sessionScope.user.id != user.id}">
                                        <form action="<c:url value="/todo/admin/users/${user.id}/delete"/>" method="post">
                                            <button class="button trash"><i class="far fa-trash-alt"></i></button>
                                        </form>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:if>
            </c:if>
        </div>
    </div>
</div>

</body>
</html>
