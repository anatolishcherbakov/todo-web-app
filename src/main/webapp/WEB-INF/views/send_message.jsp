<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>Send message</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        <%@ include file="/css/form_style.css" %>
    </style>
</head>

<body>

<div class="login-page">
    <div class="form">
        <form class="login-form" method="post" action="<c:url value="/todo/message/send"/>">
            <p class="title">Send message</p>
            <h3 style="color: red">Your account has been locked. You can send a message to clarify the reason..</h3>

            <p class="alignleft">message:</p>
            <textarea class="textarea" rows="10" cols="31" name="message" placeholder="_enter your message"></textarea>

            <input type="hidden" name="email" value="${sessionScope.user.email}">
            <button>Send</button>
            <p class="message"><a href="<c:url value="/login"/>"><-- Sign In</a></p>
        </form>
    </div>
</div>

</body>
</html>