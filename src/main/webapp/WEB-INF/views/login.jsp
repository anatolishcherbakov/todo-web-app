<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="by.it_academy.util.ApplicationConstant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In Form</title>

    <style>
        <%@ include file="../../css/form_style.css" %>
    </style>
</head>

<body>
<div class="login-page">
    <div class="form">
        <form:form class="login-form" action="${pageContext.request.contextPath}/login"
                   modelAttribute="authenticate" method="post">

            <p class="title">TO_DO</p>
            <p class="subtitle">Get clarity and calm - move your tasks from your head to the To-Do List</p>

            <jsp:include page="../../template/error_tmpl.jsp"/>

            <form:errors path="login" cssStyle="color: red"/>
            <form:input path="login" type="text" placeholder="_login"/>

            <form:errors path="password" cssStyle="color: red"/>
            <form:input path="password" type="text" placeholder="_password"/>

            <button>login</button>
            <p class="message">Not registered? <a href="<c:url value="/registration"/>">Create an account</a></p>

        </form:form>
    </div>
</div>

</body>
</html>
