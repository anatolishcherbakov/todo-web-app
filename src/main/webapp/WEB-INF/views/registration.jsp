<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.it_academy.util.ApplicationConstant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>

    <style>
        <%@ include file="../../css/form_style.css" %>
    </style>

</head>
<body>

<div class="login-page">
    <div class="form">
        <form:form class="register-form" action="${pageContext.request.contextPath}/registration"
                   modelAttribute="user" method="post">

            <p class="title">TO_DO</p>
            <p class="subtitle">Get clarity and calm - move your tasks from your head to the To-Do List</p>

            <jsp:include page="../../template/error_tmpl.jsp"/>

            <form:errors path="authenticate.login" cssStyle="color: red"/>
            <form:input path="authenticate.login" type="text" placeholder="_login"/>

            <form:errors path="authenticate.password" cssStyle="color: red"/>
            <form:input path="authenticate.password" type="text" placeholder="_password"/>

            <form:errors path="name" cssStyle="color: red"/>
            <form:input path="name" type="text" placeholder="_name"/>

            <form:errors path="surname" cssStyle="color: red"/>
            <form:input path="surname" type="text" placeholder="_surname"/>

            <form:errors path="email" cssStyle="color: red"/>
            <form:input path="email" type="email" placeholder="_email"/>

            <form:hidden path="role" value="${user.role}"/>
            <form:hidden path="authenticate.profileEnable" value="${user.authenticate.profileEnable}"/>

            <button>Registration</button>
            <p class="message">Already registered? <a href="<c:url value="/login"/>">Sign In</a></p>
        </form:form>
    </div>
</div>

</body>
</html>
