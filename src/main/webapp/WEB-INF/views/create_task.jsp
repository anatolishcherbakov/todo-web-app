<%@ page import="by.it_academy.util.ApplicationConstant" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>${requestScope.title}</title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        <%@ include file="/css/form_style.css" %>
    </style>
</head>

<body>

<div class="login-page">
    <div class="form">
        <form class="login-form" enctype="multipart/form-data" method="post"
                <c:if test="${requestScope.title == 'Add new task'}">
                    action="<c:url value="/todo/tasks/${mark}/create"/>"
                </c:if>
                <c:if test="${requestScope.title == 'Edit task'}">
                    action="<c:url value="/todo/tasks/${requestScope.task.id}/${mark}/edit"/>"
                </c:if>
        >
            <p class="title">${requestScope.title}</p>

            <p class="alignleft">name:</p>
            <input type="text" name="name" placeholder="_name"
                   value="<c:if test="${requestScope.task.name != null}">${requestScope.task.name}</c:if>"/>

            <p class="alignleft">description:</p>
            <textarea class="textarea" rows="6" cols="31" name="description" placeholder="_description"><c:if
                    test="${requestScope.task.description != null}">${requestScope.task.description}</c:if></textarea>


            <c:if test="${mark == 'some' || requestScope.title == 'Edit task'}">
                <p class="alignleft pdate">date:</p>
                <input type="date" name="date" value="${requestScope.task.date}${requestScope.date}"/>
            </c:if>

            <c:if test="${requestScope.title == 'Add new task'}">
                <p class="alignleft pfile">file:</p>
                <input type="file" name="file">
                <button>create</button>
            </c:if>

            <c:if test="${requestScope.title == 'Edit task'}">
                <button>edit</button>
            </c:if>

            <p class="message"><a href="<c:url value="/todo/tasks/${mark}"/>"><-- main page</a></p>

        </form>
    </div>
</div>

</body>
</html>
